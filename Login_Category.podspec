
Pod::Spec.new do |s|
  s.name         = "Login_Category"
  s.version      = "0.1.0"
  s.summary      = "Login_Category."
  s.homepage     = "https://gitee.com/xuanxiuiOS/Login_Category"
  s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "fun." => "yupeng_ios@163.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://gitee.com/xuanxiuiOS/Login_Category.git", :tag => s.version.to_s }
  s.source_files  = "Login_Category/Login_Category/**/*.{h,m}"
  s.dependency 'Lothar', '~> 1.0.6'
  s.requires_arc = true
end
