//
//  AppDelegate.h
//  Login_Category
//
//  Created by 于鹏 on 2017/8/11.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

