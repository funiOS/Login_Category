//
//  main.m
//  Login_Category
//
//  Created by 于鹏 on 2017/8/11.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
