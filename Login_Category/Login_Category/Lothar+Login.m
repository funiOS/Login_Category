//
//  Lothar+Login.m
//  Login_Category
//
//  Created by 于鹏 on 2017/8/11.
//  Copyright © 2017年 YIVIEW. All rights reserved.
//

#import "Lothar+Login.h"

NSString * const kLotharTargetLogin = @"Login";
NSString * const kLotharActionLoginViewController = @"loginViewController";

@implementation Lothar (Login)

- (UIViewController *)login_aViewController {
    /*XLLoginViewController *loginVC = [[XLLoginViewController alloc]init];*/
    return [self performTarget:kLotharTargetLogin action:kLotharActionLoginViewController params:nil shouldCacheTarget:NO];
}

@end
